<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register User</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h2>Register User for {{ $user->email }}</h2>
        <form action="{{ route('registeruser') }}" method="post" class="form-group">
            <input type="hidden" name="email" id="email" value="{{ $user->email }}">
            <div class="form-group">
                <input type="text" name="user_name" id="user_name" class="form-control field"
                    placeholder="Enter User Name">
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control field"
                    placeholder="Enter Password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</body>

</html>
