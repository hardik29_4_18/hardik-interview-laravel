<?php

namespace App\Http\Controllers;

use App\Mail\SendEmail;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public $successStatus = 200;
    public function __construct()
    {
        $this->apiToken = Str::random(60);
        $this->verifyPin = mt_rand(10000, 999999);
    }


    public function index(Request $request)
    {
        try {
            $admin_email = $request->admin_email;
            $user_email = $request->email;
            $adminData = User::where(['email' => $admin_email, 'role' => 1])->first();
            // dd($adminData);
            if ($adminData) {
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                ]);


                if ($validator->fails()) {
                    return sendResponse(422, 'Validation Error', $validator->errors());
                }

                $input = $request->all();
                $input['password'] = Hash::make('123456');
                $url_token = $this->apiToken;
                $input['url_token'] = $url_token;
                $input['email'] = $user_email;
                $user = User::create($input);

                $data = ['email' => $user_email, 'sign_up_url' => route('signup', ['url_token' => $url_token])];

                $data['email'] = $user_email;
                $data['username'] = "Hardik Davra";
                $data['subject'] = 'Invitation';
                $data['template'] = 'invitationmail';
                $statusCode = sendMail($data['subject'], $data, $data['email']);

                // $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['status'] = $statusCode;
                $success['email'] =  $user->email;



                if ($statusCode == 202) {
                    return sendResponse($statusCode, 'Request Successfully sent', $success);
                }

                return sendResponse($statusCode, 'Error in sending E-mail', []);
            }

            return sendResponse(401, 'You are unauthenticated person.', []);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $url_token = $request->url_token;
        $user = User::where('url_token', $url_token)->first();
        if ($user) {
            if ($user->is_url_token_expired == 0) {
                return view('registeruser', compact('user'));
            }
            return sendResponse(200, 'URL has been expired', []);
        } else {
            return sendResponse(200, 'Unauthenticaqted', 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'user_name' => 'required|max:25',
            'password' => 'required|max:15',
            'url_token' => 'required',
        ]);


        if ($validator->fails()) {
            return sendResponse(422, 'Validation Error', $validator->errors());
        }

        $user_email = $request->email;
        $user_name = $request->user_name;
        $password = $request->password;
        $url_token = $request->url_token;


        $obj_user = User::where('email', $user_email)->first();
        if ($obj_user) {
            $obj_user->user_name = $user_name;
            $obj_user->password = Hash::make($password);
            $verifyPin = $this->verifyPin;
            $obj_user->verify_pin = $verifyPin;
            $obj_user->save();
            $data = ['email' => $user_email, 'verify_pin' => $verifyPin];
            $data['email'] = $user_email;
            $data['username'] = "Hardik Davra";
            $data['subject'] = 'Verify Email';
            $data['template'] = 'verifyemail';
            $statusCode = sendMail($data['subject'], $data, $data['email']);
            return response()->json(['Success' => "Verify Pin sent to mail Successfully !!"], $this->successStatus);
        }
    }

    public function confirmUser(Request $request)
    {
        // dd($request->all());
        $user_email = $request->email;
        $verify_pin = $request->verify_pin;
        $obj_user = User::where(['email' => $user_email, 'verify_pin' => $verify_pin])->first();
        if ($obj_user) {
            $obj_user->email_verified_at = date('Y-m-d H:i:s');
            $obj_user->remember_token = $this->apiToken;
            $obj_user->save();

            return response()->json(['Success' => "User Registered Successfully !!"], $this->successStatus);
        } else {
            return response()->json(['error' => "Invalid Pin"], 401);
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
