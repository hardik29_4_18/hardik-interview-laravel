<?php

use Illuminate\Support\Facades\Log;
use SendGrid\Mail\Mail;

function sendMail($subject, $data, $tomail)
{
    try {
        $view_content = \View::make($data['template'], $data)->render();
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(config('app.frommail'), "hardikdavra2903@gmail.com");
        $email->setSubject($subject);
        $email->addTo($tomail);
        $email->addContent("text/html", $view_content);
        $sendgrid = new \SendGrid(config('app.sendgrid_api_key'));
        try {
            $response = $sendgrid->send($email);
            Log::info("Send email to " . $tomail);
            return $response->statusCode();
        } catch (Exception $e) {
            return $e->statusCode();
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    } catch (Exception $e) {
        Log::info('Caught exception: ' . $e->getMessage() . "\n");
        return 'errorCode';
    }
}


function sendResponse($status, $message, $data) {
    return response()->json([
        'status' => $status,
        'message' => $message,
        'data' => $data
    ]);
}